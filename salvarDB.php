<?php
	require('core.php');
	
	$core = new Core();
	$arrayLista = $core->carregarDados();
	
	function gravarDados($arrayLista){
		$con = new PDO("mysql:host=localhost;dbname=hogwarts", "root", ""); 
		$stmt = $con->prepare("INSERT INTO lista_compra(mes, produto, id_categoria, quantidade) VALUES(?, ?, ?, ?)");

		foreach ($arrayLista as $keyMes => $mes) {
			foreach ($mes as $keyCategoria => $categoria) {			
				foreach ($categoria as $keyProduto => $produto) {							
					$items = [
				        'Brócolis' => 'Brocolis',
				        'Papel Higiênico' => 'Papel Hignico' ,
				        'Chocolate ao leite' => 'Chocolate ao leit',
				        'Sabão em pó' => 'Sabao em po',
				        'Geléia de morango' => 'Geléria de morango'
					];				

					$nomeProduto = array_search($keyProduto, $items);

					$categorias = [
						1 => "Alimentos",
						2 => "Higiene Pessoal",
						3 => "Limpeza"
					];

					foreach ($categorias as $key => $value) {
						if($categoria == $key){
							$stmt->bindParam(3,$value);		
						}	
					}

					$stmt->bindParam(1,$mes);
					$stmt->bindParam(2,$produto);
					
					$stmt->bindParam(4,$quantidade);
											
					$stmt->execute();				
				}
			}	
		}		
	}

	gravarDados($arrayLista);
?>