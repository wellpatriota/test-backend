<?php
	require('core.php');

	$core = new Core();
	$arrayLista = $core->carregarDados();

	$items = [
        'Brócolis' => 'Brocolis',
        'Papel Higiênico' => 'Papel Hignico' ,
        'Chocolate ao leite' => 'Chocolate ao leit',
        'Sabão em pó' => 'Sabao em po',
        'Geléia de morango' => 'Geléria de morango'
	];

	$fp = fopen('lista-de-compras.csv', 'w');

	$columnName = $core->convertToISOCharset(['Mês', 'Categoria', 'Produto', 'Quantidade']);
	fputcsv($fp, $columnName);

	foreach ($arrayLista as $keyMes => $mes) {
		foreach ($mes as $keyCategoria => $categoria) {			
			foreach ($categoria as $keyProduto => $produto) {						
				$nomeProduto = array_search($keyProduto, $items);

				if($nomeProduto){
					fputcsv($fp, $core->convertToISOCharset([$keyMes, $keyCategoria, $nomeProduto, $produto]));	
				}else{
					fputcsv($fp, $core->convertToISOCharset([$keyMes, $keyCategoria, $keyProduto, $produto]));
				}				
			}
		}	
	}			

	fclose($fp);	
?>

