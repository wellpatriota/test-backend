-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 30-Mar-2019 às 01:53
-- Versão do servidor: 5.5.62
-- versão do PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hogwarts`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_produto`
--

DROP TABLE IF EXISTS `categoria_produto`;
CREATE TABLE IF NOT EXISTS `categoria_produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria_produto`
--

INSERT INTO `categoria_produto` (`id`, `nome`) VALUES
(1, 'Alimentos'),
(2, 'Higiene Pessoal'),
(3, 'Limpeza');

-- --------------------------------------------------------

--
-- Estrutura da tabela `lista_compra`
--

DROP TABLE IF EXISTS `lista_compra`;
CREATE TABLE IF NOT EXISTS `lista_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mes` varchar(50) NOT NULL,
  `produto` varchar(255) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `lista_compra`
--

INSERT INTO `lista_compra` (`id`, `mes`, `produto`, `id_categoria`, `quantidade`) VALUES
(1, 'test', 'test', 3, 3),
(2, 'test', 'test', 3, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

DROP TABLE IF EXISTS `produto`;
CREATE TABLE IF NOT EXISTS `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_categoria` (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `nome`, `id_categoria`) VALUES
(1, 'Pão de forma', 1),
(2, 'Nutella', 1),
(3, 'Arroz', 1),
(4, 'Feijão', 1),
(5, 'Veja Multiuso', 3),
(6, 'Sabão em pó', 3),
(7, 'Desinfetante', 3),
(8, 'Creme dental', 2),
(9, 'Sabonete Protex', 2),
(10, 'Escova de Dente', 2),
(11, 'Papel Higiênico', 2),
(12, 'Ovos', 1),
(13, 'Iogurte', 1),
(14, 'Pasta de Amendoim', 1),
(15, 'Detergente', 3),
(16, 'Enxaguante bucal', 2),
(17, 'Brócolis', 1),
(18, 'Tomate', 1),
(31, 'Morango', 1),
(32, 'Beringela', 1),
(33, 'Pepino', 1),
(34, 'Arroz integral', 1),
(35, 'Filé de frango', 1),
(36, 'Leite Desnatado', 1),
(37, 'Diabo verde', 3),
(38, 'MOP', 3),
(39, 'Queijo Minas', 1),
(40, 'Protex', 2),
(41, 'Shampoo', 2),
(42, 'Pão de forma', 1),
(43, 'geléia de morango', 1),
(44, 'Pano de chão', 3),
(45, 'Rodo', 3),
(47, 'Sabonete Dove', 2),
(48, 'Doritos', 1),
(49, 'Chocolate ao leite', 1),
(50, 'Filé Mignon', 1),
(51, 'Esponja de aço', 3),
(52, 'Fio Dental', 2),
(53, 'Escova de dente', 2);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `produto_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria_produto` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
