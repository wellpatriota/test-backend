<?php
	Class Core{
		public function carregarDados(){
			$items = [
		        'Brócolis' => 'Brocolis',
		        'Papel Higiênico' => 'Papel Hignico' ,
		        'Chocolate ao leite' => 'Chocolate ao leit',
		        'Sabão em pó' => 'Sabao em po',
		        'Geléia de morango' => 'Geléria de morango'
			];

			$arrayLista = $this->ordenarPorMes(include("lista-de-compras.php"));

			return $arrayLista;
		}

		public function convertToISOCharset($array){
		    foreach($array as $key => $value)
		    {
		        if(is_array($value)){
		            $array[$key] = convertToISOCharset($value);
		        }else{
		            $array[$key] = mb_convert_encoding($value, 'ISO-8859-1', 'UTF-8');
		        }
		    }

		    return $array;
		}

		private function ordenarPorMes($listaMes){
			$meses = [
				01 => 'janeiro',
				02 => 'fevereiro',
				03 => 'marco',
				04 => 'abril',
				05 => 'maio',
				06 => 'junho'
			];

			foreach ($meses as $key => $mes) {
				$newArray[$mes] = $this->ordenarPorCategoria($listaMes[$mes]);			
			}

			return $newArray;
		}

		private function ordenarPorCategoria($listaCategoria){
			$categorias = [
				01 => 'alimentos',
				02 => 'higiene_pessoal',
				03 => 'limpeza'
			];

			foreach ($categorias as $key => $categoria) {
				//$newArray[$categoria] = $this->ordenarPorQuantidade($listaCategoria[$categoria]);
				$newArray[$categoria] = $listaCategoria[$categoria];
				//var_dump($newArray[$categoria]);die();
			}

			return $newArray;
		}

		private function ordenarPorQuantidade($listaQuantidade){	
			$chaves = array_keys($listaQuantidade);
			
			$newArray = [];
			foreach ($chaves as $chave) {
				$aux = [];
				foreach ($listaQuantidade as $key => $value) {
					if($listaQuantidade[$key] >= $listaQuantidade[$chave]){
						$aux = $listaQuantidade[$chave];
					}else{
						$aux = $listaQuantidade[$key];
					}
				}	
				$newArray[] = $aux;
			}		
		}
	}
?>